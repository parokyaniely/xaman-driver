# README #

* Official repository for the driver client prototype for the Xamanek System
* Version 1.0

### How do I get set up? ###

1. Clone repo
2. Sync Gradle
3. Make sure Android Studio is 3.0 upwards
4. Build project
5. Allow all permissions if your device is on Marshmallow and up

### Contribution guidelines ###

1. Adhere to these guidelines: https://github.com/rockycamacho/android-guidelines
2. Address your pull requests to me.
3. Branch out from `develop`

### Who do I talk to? ###

* Monica Labbao: medium.com/@ma.labbao
* Elymar Apao: medium.com/@jozzua