package com.xaman.driver;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.xaman.driver.databinding.ActivityMainBinding;
import com.xaman.driver.model.BusLocationStatus;
import com.xaman.driver.service.SendBusLocationPresenter;
import com.xaman.driver.service.SendBusLocationView;

public class MainActivity extends AppCompatActivity implements SendBusLocationView {

    private ActivityMainBinding binding;
    private SendBusLocationPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        binding.tablayout.setupWithViewPager(binding.pager);
        binding.pager.setAdapter(new MainPagerAdapter(getSupportFragmentManager()));
        binding.tablayout.getTabAt(0).setText("Route");
        binding.tablayout.getTabAt(1).setText("Map");
        presenter = new SendBusLocationPresenter(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.updateLocation();
    }

    @Override
    public void onBusLocationUpdated(BusLocationStatus status) {
        Toast.makeText(this, "Your bus location is updated", Toast.LENGTH_SHORT).show();
        presenter.updateLocation();
    }

    @Override
    public void onBusLocationUpdateFailed(String error, Throwable t) {
        presenter.updateLocation();
    }

    @Override
    public Context getContext() {
        return this;
    }
}
