package com.xaman.driver;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.xaman.driver.map.MapFragment;
import com.xaman.driver.route.RouteFragment;

import java.util.ArrayList;

/**
 * Created by monica on 12/30/17.
 */

public class MainPagerAdapter extends FragmentPagerAdapter {
    private FragmentManager supportFragmentManager;
    ArrayList<Fragment> fragments;

    public MainPagerAdapter(FragmentManager supportFragmentManager) {
        super(supportFragmentManager);
        this.supportFragmentManager = supportFragmentManager;
        fragments = new ArrayList<>();
        initFragments();
    }

    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }

    private void initFragments() {
        fragments.add(new RouteFragment());
        fragments.add(new MapFragment());
    }
}
