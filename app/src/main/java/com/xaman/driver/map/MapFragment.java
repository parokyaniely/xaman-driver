package com.xaman.driver.map;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.xaman.driver.R;
import com.xaman.driver.databinding.FragmentMapBinding;

/**
 * Created by monica on 12/30/17.
 */

public class MapFragment extends Fragment implements OnMapReadyCallback, OnSuccessListener<Location> {
    private FragmentMapBinding binding;
    private GoogleMap googleMap;
    private FusedLocationProviderClient fusedLocationClient;
    private LocationManager locationManager;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_map, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.map.onCreate(savedInstanceState);
        initMap();
    }

    @Override
    public void onResume() {
        super.onResume();
        binding.map.onResume();
    }

    private void initMap() {
        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }
        binding.map.getMapAsync(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        binding.map.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        binding.map.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        binding.map.onLowMemory();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        this.googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(getActivity(), R.raw.map_style));
        this.googleMap.setMyLocationEnabled(true);
        /*fusedLocationClient = LocationServices.getFusedLocationProviderClient(getContext());
        fusedLocationClient.getLastLocation().addOnSuccessListener(this);*/
        locationManager = ((LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE));
        Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        onSuccess(location);
        setSampleBuses();
    }

    @Override
    public void onSuccess(Location location) {
        if (location != null) {
            //current location
//            LatLng currentLocation = new LatLng(location.getLatitude(), location.getLongitude());
            //Shaw Blvd cor. EDSA
            LatLng currentLocation = new LatLng(14.581079, 121.053478);
            CameraPosition cameraPosition = new CameraPosition.Builder().target(currentLocation).zoom(18).build();
            googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        }
    }

    //TODO: look for current location
    private void showLocation() {
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        fusedLocationClient.getLastLocation();
    }

    private void setSampleBuses() {
        googleMap.addMarker(new MarkerOptions()
                .title("Five Star Bus: Cubao - Dagupan vv")
                .position(new LatLng(14.579978, 121.052816)));
        googleMap.addMarker(new MarkerOptions()
                .title("Lippad Transit: Alabang - Novaliches via Commonwealth")
                .position(new LatLng(14.579635, 121.052612)));
        googleMap.addMarker(new MarkerOptions()
                .title("Santrans: Manila - Norzagaray via NLEX")
                .position(new LatLng(14.578846, 121.052108)));
        googleMap.addMarker(new MarkerOptions()
                .title("Jayross Tours: Baclaran - Tungko via Ayala, Commonwealth")
                .position(new LatLng(14.578784, 121.052044)));
        googleMap.addMarker(new MarkerOptions()
                .title("JAM Liner: SM City Lipa - Cubao via Buendia")
                .position(new LatLng(14.582064, 121.054433)));
    }

}
