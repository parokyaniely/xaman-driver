package com.xaman.driver.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by monica on 12/31/17.
 */

public class Bus implements Parcelable {
    @SerializedName("latlong")
    @Expose
    public String latlong;
    @SerializedName("marks")
    @Expose
    public Integer marks;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("people")
    @Expose
    public Integer people;

    protected Bus(Parcel in) {
        latlong = in.readString();
        if (in.readByte() == 0) {
            marks = null;
        } else {
            marks = in.readInt();
        }
        name = in.readString();
        if (in.readByte() == 0) {
            people = null;
        } else {
            people = in.readInt();
        }
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(latlong);
        if (marks == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(marks);
        }
        dest.writeString(name);
        if (people == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(people);
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Bus> CREATOR = new Creator<Bus>() {
        @Override
        public Bus createFromParcel(Parcel in) {
            return new Bus(in);
        }

        @Override
        public Bus[] newArray(int size) {
            return new Bus[size];
        }
    };

    public String getLatlong() {
        return latlong;
    }

    public void setLatlong(String latlong) {
        this.latlong = latlong;
    }

    public Integer getMarks() {
        return marks;
    }

    public void setMarks(Integer marks) {
        this.marks = marks;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPeople() {
        return people;
    }

    public void setPeople(Integer people) {
        this.people = people;
    }
}
