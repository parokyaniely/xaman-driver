package com.xaman.driver.model;

/**
 * Created by monica on 12/31/17.
 */

public class BusLocation {
    private String latlong;

    public String getLatlong() {
        return latlong;
    }

    public void setLatlong(String latlong) {
        this.latlong = latlong;
    }
}
