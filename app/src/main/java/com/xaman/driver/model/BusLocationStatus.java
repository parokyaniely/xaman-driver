package com.xaman.driver.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by monica on 12/31/17.
 */

public class BusLocationStatus implements Parcelable {
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("bus")
    @Expose
    public Bus bus;

    protected BusLocationStatus(Parcel in) {
        status = in.readString();
        bus = in.readParcelable(Bus.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(status);
        dest.writeParcelable(bus, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<BusLocationStatus> CREATOR = new Creator<BusLocationStatus>() {
        @Override
        public BusLocationStatus createFromParcel(Parcel in) {
            return new BusLocationStatus(in);
        }

        @Override
        public BusLocationStatus[] newArray(int size) {
            return new BusLocationStatus[size];
        }
    };
}
