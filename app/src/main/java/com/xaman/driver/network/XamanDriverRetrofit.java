package com.xaman.driver.network;

import android.util.Log;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by monica on 12/31/17.
 */

public class XamanDriverRetrofit {
    private Retrofit xamanRetrofit;

    public XamanDriverRetrofit() {
        initRetrofit();
    }

    private void initRetrofit() {
        xamanRetrofit = new Retrofit.Builder()
                .baseUrl("http://xamanek.magis.solutions")
                .addConverterFactory(GsonConverterFactory.create())
                .client(getClient())
                .build();
    }

    public Retrofit getXamanRetrofit() {
        return xamanRetrofit;
    }

    public OkHttpClient getClient() {
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(new HttpLoggingInterceptor(new HttpLoggingInterceptor.Logger() {
                    @Override
                    public void log(String message) {
                        Log.v("XamanApi:", message);
                    }
                }).setLevel(HttpLoggingInterceptor.Level.BODY))
                .build();
        return client;
    }
}
