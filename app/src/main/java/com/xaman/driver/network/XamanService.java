package com.xaman.driver.network;

import com.xaman.driver.model.BusLocation;
import com.xaman.driver.model.BusLocationStatus;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.PUT;
import retrofit2.http.Path;

/**
 * Created by monica on 12/31/17.
 */

public interface XamanService {
    @PUT("bus/{id}")
    Call<BusLocationStatus> updateLocationStatus(@Path("id") String id, @Body BusLocation busLocation);
}
