package com.xaman.driver.route;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.xaman.driver.R;
import com.xaman.driver.databinding.FragmentRouteBinding;

/**
 * Created by monica on 12/30/17.
 */

public class RouteFragment extends Fragment {
    private FragmentRouteBinding binding;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_route, container, false);
        return binding.getRoot();
    }
}
