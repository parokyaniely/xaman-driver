package com.xaman.driver.service;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.CountDownTimer;
import android.support.v4.app.ActivityCompat;

import com.google.android.gms.maps.model.LatLng;
import com.xaman.driver.model.BusLocation;
import com.xaman.driver.model.BusLocationStatus;
import com.xaman.driver.network.XamanDriverRetrofit;
import com.xaman.driver.network.XamanService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by monica on 12/31/17.
 */

public class SendBusLocationPresenter {
    private XamanService xamanService;
    private SendBusLocationView view;
    private LocationManager locationManager;

    public SendBusLocationPresenter(SendBusLocationView view) {
        this.view = view;
        XamanDriverRetrofit retrofit = new XamanDriverRetrofit();
        xamanService = retrofit.getXamanRetrofit().create(XamanService.class);
        sendLocation();
    }

    /**TODO: Consider using a BroadcastReceiver or a custom service instead.
     * */
    public void updateLocation() {
        new CountDownTimer(25000,1000){
            @Override
            public void onTick(long l) {

            }

            @Override
            public void onFinish() {
                sendLocation();
            }
        }.start();
    }

    private void sendLocation() {
        locationManager = ((LocationManager) view.getContext().getSystemService(Context.LOCATION_SERVICE));
        if (ActivityCompat.checkSelfPermission(view.getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(view.getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        if (location != null) {
            BusLocation newBusLoc = new BusLocation();
            LatLng currentLocation = new LatLng(14.581092, 121.053455);
            newBusLoc.setLatlong(currentLocation.latitude + "," +  currentLocation.longitude);
            xamanService.updateLocationStatus("victoryliner", newBusLoc).enqueue(new Callback<BusLocationStatus>() {
                @Override
                public void onResponse(Call<BusLocationStatus> call, Response<BusLocationStatus> response) {
                    if (response.code() == 200) {
                        view.onBusLocationUpdated(response.body());
                    } else {
                        view.onBusLocationUpdateFailed(response.errorBody().toString(), new Exception());
                    }
                }

                @Override
                public void onFailure(Call<BusLocationStatus> call, Throwable t) {
                    view.onBusLocationUpdateFailed(t.getMessage(), t);
                }
            });
        }
    }
}
