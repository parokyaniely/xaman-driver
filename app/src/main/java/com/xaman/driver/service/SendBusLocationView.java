package com.xaman.driver.service;

import android.content.Context;

import com.xaman.driver.model.BusLocationStatus;

/**
 * Created by monica on 12/31/17.
 */

public interface SendBusLocationView {
    void onBusLocationUpdated(BusLocationStatus status);
    void onBusLocationUpdateFailed(String error, Throwable t);
    Context getContext();
}
